package net.flowas.modulecart.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.flowas.modulecart.domain.Category;
@Api("/categories")
@Path("/categories")
public interface CategoryEndpoint extends CrudEndpoint<Category>{
	@GET
	@Path("/{id:[0-9][0-9]*}")
	@Produces("application/json")
	@ApiOperation(value = "产品目录")
	Category findById(@PathParam("id") Long id);
	@GET
	@Path("list.json")
	@Produces("application/json")
	@ApiOperation(value = "产品目录")
	List<Category> listAll(@QueryParam("start") Integer startPosition,
			@QueryParam("max") Integer maxResult);

}