package net.flowas.modulecart.rest.impl;

import net.flowas.modulecart.rest.CategoryEndpoint;
import net.flowas.modulecart.rest.ConfigerationEndpoint;
import net.flowas.modulecart.rest.ProductEndpoint;
import net.flowas.modulecart.rest.RootRest;

public class RootRestImpl implements RootRest {

	CategoryEndpoint categoryEndpoint=new CategoryImpl();
	ProductEndpoint productEndpoint=new ProductImpl();
	ConfigerationEndpoint configerationEndpoint=new ConfigerationImpl();
	@Override
	public CategoryEndpoint getCategoryEndpoint() {
		return categoryEndpoint;
	}

	@Override
	public ProductEndpoint getProductEndpoint() {
		return productEndpoint;
	}

	@Override
	public ConfigerationEndpoint getConfigerationEndpoint() {
		return configerationEndpoint;
	}
}
