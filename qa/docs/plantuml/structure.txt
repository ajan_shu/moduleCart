@startuml

[Combined Web] as combinedWeb
[FrontOffice UI] as frontOffice
[Shop Engine] as shopEngine
combinedWeb ..> shopEngine : include 
combinedWeb ..> frontOffice : include 
combinedWeb ..> [Identity Management] : include 
frontOffice ..> shopEngine : dataExchange"
[Search Engine] ..> shopEngine : dataExchange
[Integration Center] ..> shopEngine : dataSynchronize

@enduml